interface IRepository {
    create(params?: any): any
    update(params?: any): any
    get(params?: any): any
    delete(params?: any): boolean
};

export default IRepository;