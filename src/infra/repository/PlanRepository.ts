import IRepository from './IRepository';
import { IPlan } from 'src/core/plan/IPlan';
import { IFullConditions } from 'src/core/IConditions';

class PlanRepository implements IRepository {
    private plans: IPlan[] = [
        {
            name: 'FaleMais 30',
            timeFree: 30,
        },
        {
            name: 'FaleMais 60',
            timeFree: 60,
        },
        {
            name: 'FaleMais 120',
            timeFree: 120,
        }
    ];

    create() {
        throw "Not implementd";
    }
    
    update() {
        throw "Not implementd";
    }

    get(): IPlan[] {
        return this.plans;
    }

    getOne(conditions: IFullConditions): IPlan|Error {
        const { namePlan } = conditions;
        if (namePlan) {
            return this.plans.find((plan) => plan.name === namePlan);
        } else {
            throw 'Required param namePlan';
        }
    }

    delete() {
        return false;
    }
}

export default PlanRepository;