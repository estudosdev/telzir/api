import IRepository from './IRepository';
import { ICall } from 'src/core/call/ICall';
import { IBasicCondition } from 'src/core/IConditions';

class CallRepository implements IRepository {
    private calls: ICall[] = [
        {
            origin: '011',
            destination: '016',
            perMinute: 1.90,
        },
        {
            origin: '016',
            destination: '011',
            perMinute: 2.90,
        },
        {
            origin: '011',
            destination: '017',
            perMinute: 1.70,
        },
        {
            origin: '017',
            destination: '011',
            perMinute: 2.70,
        },
        {
            origin: '011',
            destination: '018',
            perMinute: 0.90,
        },
        {
            origin: '018',
            destination: '011',
            perMinute: 1.90,
        },
    ]

    create() {
        throw "Not implementd";
    }
    
    update() {
        throw "Not implementd";
    }

    get(): ICall[] {
        return this.calls;
    }

    getOne({ origin, destination }: IBasicCondition): ICall|Error {
        if (origin && destination) {
            return this.calls.find(
                (call) => call.origin === origin && call.destination === destination
            );
        } else {
            throw 'Required params from and to';
        }
    }

    delete() {
        return false;
    }
}

export default CallRepository;