import { ICall } from 'src/core/call/ICall';
import { IPlan } from 'src/core/plan/IPlan';
import { IBasicCondition, IFullConditions } from 'src/core/IConditions';

export interface ICallRespository {
    create(params?: any): any
    update(params?: any): any
    get(): ICall[]
    getOne(params?: IBasicCondition): ICall
    delete(params?: any): boolean
};


export interface IPlanRespository {
    create(params?: any): any
    update(params?: any): any
    get(): IPlan[]
    getOne(params?: IFullConditions): IPlan
    delete(params?: any): boolean
};