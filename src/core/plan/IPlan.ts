export interface IPlan {
    name: string
    timeFree: number
}

export interface IPlanService {
    calculate(conditions?)
}