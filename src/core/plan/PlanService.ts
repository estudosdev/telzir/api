import { IFullConditions } from 'src/core/IConditions';
import { IPlan, IPlanService } from './IPlan';
import { ICallService, ICall } from 'src/core/call/ICall';
import { IPlanRespository } from 'src/core/Protocols';

const RATE = 0.10;

class PlanService implements IPlanService {
    private callService: ICallService;
    private planRepository: IPlanRespository;

    constructor({ callService, planRepository }) {
        this.callService = callService;
        this.planRepository = planRepository;
    }

    public calculate(conditions: IFullConditions) {
        const plan: IPlan = this.planRepository.getOne(conditions);
        const call = this.callService.getCall(conditions);
        return {
            price: this.getPrice(plan, call, conditions)
        };
    }
    
    private getPrice(plan: IPlan, { perMinute }: ICall, { minute }: IFullConditions ): number {
        if (minute > plan.timeFree) {
            const excedent = minute - plan.timeFree;
            const price = excedent * perMinute;
            const rate = price * RATE;
            const priceWithRate = rate + price;
            return priceWithRate;
        } else {
            return 0
        }
    }
}

export default PlanService;