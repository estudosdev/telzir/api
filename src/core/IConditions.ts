export interface IBasicCondition {
    origin: string
    destination: string
    minute: number
}

export interface IFullConditions extends IBasicCondition {
    namePlan: string
};