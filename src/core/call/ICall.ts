export interface ICall {
    origin: string
    destination: string
    perMinute: number
}

export interface ICallService {
    getCall(conditions?: any): ICall
    getCalls(): ICall[]
    calculate(conditions?: any): any
}