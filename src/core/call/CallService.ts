import { ICall, ICallService } from './ICall';
import { IBasicCondition } from 'src/core/IConditions';
import { ICallRespository } from 'src/core/Protocols';

class Call implements ICallService {
    private callRepository: ICallRespository;

    constructor({ callRepository }) {
        this.callRepository = callRepository;
    }

    public calculate({ origin, destination, minute }: IBasicCondition) {
        const call: ICall = this.callRepository.getOne({ origin, destination, minute });
        
        return {
            price: call.perMinute * Number(minute),
        }
    }

    public getCalls(): ICall[] {
        return this.callRepository.get();
    }

    public getCall({ origin, destination, minute }: IBasicCondition): ICall {
        return this.callRepository.getOne({ origin, destination, minute });
    }
}

export default Call;