import { createContainer, asClass, asFunction } from 'awilix';

import Application from './Application';

import CallRepository from 'src/infra/repository/CallRepository';
import PlanRepository from 'src/infra/repository/PlanRepository';

import CallService from 'src/core/call/CallService';
import PlanService from 'src/core/plan/PlanService';

import GetSimulation from 'src/application/GetSimulation';

import HttpRoutes from 'src/interface/net/http/Routes';
import HttpServer from 'src/interface/net/http/Server';

const container = createContainer();

// Aplications
container.register({
    getSimulation: asClass(GetSimulation).singleton(),
});

// Infra
container.register({
    callRepository: asClass(CallRepository).singleton(),
    planRepository: asClass(PlanRepository).singleton(),
});

// core
container.register({
    callService: asClass(CallService).singleton(),
    planService: asClass(PlanService).singleton(),
});

// Interface
container.register({
    httpRoutes: asFunction(HttpRoutes).singleton(),
    httpServer: asClass(HttpServer).singleton(),
});

// System
container.register({
    app: asClass(Application).singleton(),
});

export default container;
