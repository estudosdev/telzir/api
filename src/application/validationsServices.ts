const invalidValue = param => (param === null || param === undefined);


export const validateRequiredParameters = (params) => {
    const invalidKeys = Object.keys(params).filter(key => invalidValue(params[key]));
    return Boolean(invalidKeys.length);
  }

