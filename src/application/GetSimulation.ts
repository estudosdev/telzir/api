import IApplication from './IApplication';
import { ICallService } from 'src/core/call/ICall';
import { IPlanService } from 'src/core/plan/IPlan';
import { IFullConditions } from 'src/core/IConditions';
import { validateRequiredParameters } from './validationsServices';

class GetSimulation implements IApplication {
    private callService: ICallService;
    private planService: IPlanService;

    constructor({ callService, planService }) {
        this.callService = callService;
        this.planService = planService;
    }

    public async execute(conditions: IFullConditions) {
        try {
            const { origin, destination, minute } = conditions;

            if ( validateRequiredParameters( { origin, destination, minute } ) ) {
                return this.callService.getCalls();
            } else {
                const planPrice = this.planService.calculate(conditions);
                const callPrice = this.callService.calculate(conditions);
                return {
                    withPlan: planPrice,
                    withOutPlan: callPrice
                }
            }
        } catch (error) {
            throw error;
        }
    }
};

export default GetSimulation;