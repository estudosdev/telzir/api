interface IApplication {
    execute: (params?) =>  Promise<any>;
}

export default IApplication;
