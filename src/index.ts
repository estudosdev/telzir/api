//TODO move to /src
import container from './container';
import Application from './Application';

const app: Application = container.resolve('app');

app.start();
