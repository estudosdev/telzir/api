import express, { Router, Application } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import IServer from 'src/interface/IServer';

class Server implements IServer {
    private express: Application;
    private routes: Router;

    public constructor({ httpRoutes }) {
        this.routes = httpRoutes;
        this.express = express();
        this.config();
    }

    public start(): Promise<any> {
        return new Promise(async (resolve) => {
            this.express.listen(process.env.APPLICATION_PORT, () => {
                resolve();
            });
        });
    }

    private config(): void {
        this.express.use(cors());
        this.express.use(bodyParser.urlencoded({ extended: true }));
        this.express.use(bodyParser.json());
        this.express.use(this.routes);
    }
}

export default Server;
