import { Router, Response } from 'express';
import httpStatus from 'http-status';

export = ({ getSimulation }): Router => {
    const router = Router();

    router.get('/', (_, res: Response) => {
        return res.status(httpStatus.OK).json({
            STATUS: 'OK',
        });
    });

    router.get('/simulation', async (req, res) => {
        try {
            const { query: conditions } = req;
        
            const simulation = await getSimulation.execute(conditions);
            
            return res.status(httpStatus.OK).json({
                simulation
            });
        } catch (error) {
            return res.status(httpStatus.BAD_REQUEST).json({
                error: 'The simulation could not be performed, please try again or contact us'
            })
        }
    });

    return router;
};
