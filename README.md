
**These instructions will get you a copy of the project up and running on your local machine for development and testing purposes**

- [Pre-requisites]
    - [Docker]
- [Patterns]
    - [Chain - https://refactoring.guru/design-patterns/chain-of-responsibility]
- [Architecture]
    - [Clean Code[
    - [Clean Architecture[

## Docker

For development we're using docker with node 13.

## Developing

The port of this appplication is predefined as `3009`

## Getting Started

### Installing
```
    docker-compose run app ash
    npm install
```

Exit the container by typing:

```
    exit
```

## Running

### Start the App

```
    docker-compose up app
```

### Run Tests

```
    docker-compose run tst
```

## Routes

** FETCH SIMULATION **

*GET* ```/simulation?origin=011&destination=016&minute=20&namePlan=FaleMais 30```

*200* ```OK```

*Example*

```
{
  "simulation": {
    "withPlan": {
      "price": 0
    },
    "withOutPlan": {
      "price": 38
    }
  }
}
```

** FETCH SIMULATION **

*GET* ```/simulation?origin=011&destination=011&minute=20&namePlan=FaleMais 30```

*400* ```BAD REQUEST```

*Example*

```
{
  "error": "The simulation could not be performed, please try again or contact us"
}
```