module.exports = {
    preset: 'ts-jest',
    verbose: true,
    testURL: 'http://localhost',
    collectCoverage: true,
    testEnvironment: 'node',
    setupFilesAfterEnv: [],
    testResultsProcessor: 'jest-sonar-reporter',
    cache: false,
    transform: {
        ".(ts|tsx)": "<rootDir>/tst/preprocessor.js"
    },
    testRegex: "(/__tst__/.*|\\.(test|tst))\\.(ts|tsx|js)$",
    moduleFileExtensions: [
        "ts",
        "tsx",
        "js",
        "json"
    ]
};
