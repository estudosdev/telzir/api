import { defineFeature, loadFeature } from 'jest-cucumber';
import request, { Response } from 'supertest';
import container from 'src/container';
import { IFullConditions } from 'src/core/IConditions';

const httpServer: any = container.resolve('httpServer');

const feature = loadFeature('tst/feature/features/GetSimulation.feature');

defineFeature(feature, (test) => {
    let response: Response;
    let mockConditions: IFullConditions;

    test('FaleMais 30', ({ given, when, then }) => {
        given('The plan data', () => {
            mockConditions = {
                origin: '011',
                destination: '016',
                minute: 20,
                namePlan: 'FaleMais 30',
            }
        });

        when('I choose the FaleMais 30 plan with 011 origin and 016 destination speaking 20 minutes', async () => {
            const url = '/simulation';
            response = await request(httpServer.express)
                .get(url)
                .query(mockConditions)
        });

        then('Should I return a price of zero reais', () => {
            const { body, status } = response;
            expect(status).toBe(200);
            expect(body.simulation.withOutPlan.price).toBe(38);
            expect(body.simulation.withPlan.price).toBe(0);
        });
    });

    test('FaleMais 60', ({ given, when, then }) => {
        given('The plan data', () => {
            mockConditions = {
                origin: '011',
                destination: '017',
                minute: 80,
                namePlan: 'FaleMais 60',
            }
        });

        when('I choose the FaleMais 60 plan with 011 origin and destination 017 speaking 80 minutes', async () => {
            const url = '/simulation';
            response = await request(httpServer.express)
                .get(url)
                .query(mockConditions)
        });

        then('Should I return a price of 37.4', () => {
            const { body, status } = response;
            expect(status).toBe(200);
            expect(body.simulation.withOutPlan.price).toBe(136);
            expect(body.simulation.withPlan.price).toBe(37.4);
        });
    });

    test('FaleMais 120', ({ given, when, then }) => {
        given('The plan data', () => {
            mockConditions = {
                origin: '018',
                destination: '011',
                minute: 200,
                namePlan: 'FaleMais 120',
            }
        });

        when('I choose the FaleMais 120 plan with 018 origin and destination 011 speaking 200 minutes', async () => {
            const url = '/simulation';
            response = await request(httpServer.express)
                .get(url)
                .query(mockConditions)
        });

        then('Should I return a price of 380', () => {
            const { body, status } = response;
            expect(status).toBe(200);
            expect(body.simulation.withOutPlan.price).toBe(380);
            expect(body.simulation.withPlan.price).toBe(167.2);
        });
    });
});