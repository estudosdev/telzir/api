Feature: Get Simulation

Scenario: FaleMais 30
    Given The plan data
    When I choose the FaleMais 30 plan with 011 origin and 016 destination speaking 20 minutes
    Then Should I return a price of zero reais
    
Scenario: FaleMais 60
    Given The plan data
    When I choose the FaleMais 60 plan with 011 origin and destination 017 speaking 80 minutes
    Then Should I return a price of 37.4

Scenario: FaleMais 120
    Given The plan data
    When I choose the FaleMais 120 plan with 018 origin and destination 011 speaking 200 minutes
    Then Should I return a price of 380